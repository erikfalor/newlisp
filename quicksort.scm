; Helper function for partition.
; To create partitions, we need a function which takes as parameters two extra lists:
; the "in" list and the "out" list.
;
; I don't want to force ppl to always have to call partition() with two empty
; lists, so we use this helper function to hide the extra complexity from the
; caller who gets to enjoy a simplified interface.
(define (part-helper pred? lst in out)
  (if (null? lst)
    (list in out)
    (if (pred? (car lst))
      (part-helper pred? (cdr lst) (cons (car lst) in) out)
      (part-helper pred? (cdr lst) in (cons (car lst) out)))))

; the user-friendly interface to partition. Simply supply a predicate and a list
(define (part pred? lst)
  (part-helper pred? lst '() '()))

; given a comparator and a list, return a sorted list by way of the quicksort algorithm
; the 2-arity comparator argument is made into a unary predicate and passed to partition
(define (quicksort cmp lst)
  (if (null? lst)
    '() ; base case

    (let ((pivot (car lst))
          (lst (cdr lst)))
      ; Convert a 2-arity comparator into a unary predicate by applying 1
      ; argument to a comparitor with a chosen pivot value
      (let ((pred? (lambda (n) (cmp n pivot))))

        (let ((parts (part pred? lst)))
          (append (quicksort cmp (car parts)) (list pivot) (quicksort cmp (cadr parts))))))))

