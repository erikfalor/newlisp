## newLISP manual

http://www.newlisp.org/downloads/newlisp_manual.html


## The REPL

Use ```(load "filename.lsp")``` to re-load source files into a running REPL

```!ls``` allows you to run shell commands from within the REPL

```$ newlisp -s 6000``` increases the stack size to 6k entries. By default it's something like 2k

Add ```set blink-matching-paren on``` to ~/.inputrc so the REPL can highlight matching parens.

From the REPL prompt '>' hit [Enter] to begin multi-line input mode. You can now create multi-line expressions. End multi-line input mode by pressing [Enter] again on a blank line.

Launch ```$ newlisp -p 4444``` and newLISP listens for TCP/IP connections on port 4444, presenting the client with a REPL. This process dies when the client disconnects.


## Differences

Lambda lists evaluate to themselves, so you can manipulate a function's code directly with list operators.

Lists and strings are implicitly indexed, as in Arc.

    ```("Mary had a little lamb" 3) ; => "y"```

    ```((sequence 1 8) 3) ; => 4```

    ```((sequence 1 8) -3) ; => 6```

## Compiler

Compile a script into an executable

```$ newlisp -x source.lsp output```

You must manually ```chmod +x output``` to make it executable, tho


## Function Reference

http://www.newlisp.org/downloads/newlisp_manual.html#function_ref
