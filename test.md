# CS 2610 - Fri Feb 09

## Announcements


## Questions:
    0. How do I control Django?
    1. What are all of the files Django makes for me?
    2. What's the difference between a *project* and an *app*
    3. Introduction to Python

--------------------------------------------------------------------------------
##  0. How do I control Django?

### django-admin

Initialize your Django Project with ```django-admin startproject```

django-admin is Django's command-line utility for administrative tasks.

    $ django-admin startproject cs2610-proj

This command creates a Django project directory structure for the given project
name in the current directory or the given destination.

By default, the new directory contains manage.py and a project package
(containing a settings.py and other files).


#### Create a git repo for your project

At this point it is a good idea to put some anchor points into the sheer rock cliff you're scaling

    $ cd cs2610-proj
    $ git init

There are going to be a lot of files which you *don't* want to keep in git.
Create a .gitignore file to let git know about these

    .gitignore might contain the following:
        *.pyc
        db.sqlite3

    $ git add .gitignore .
    $ git commit -m 'project initialized with .gitignore file'


--------------------------------------------------------------------------------
## 1. What are all of the files Django makes for me?

#### django-admin

The ```django-admin startproject``` command creates this directory structure

```
cs2610-proj/         # Project directory; its name doesn't really matter
	cs2610-proj/     # Project package. Django looks for settings in here
	   __init__.py
	   settings.py   # our project's settings
	   urls.py       # the "receptionist" - tells you where to go to use an app
	   wsgi.py       # Pertains to the WSGI interface to a "real" web server
	db.sqlite3       # Your SQLite database; it appears after you run a *migration*
	manage.py*       # Basically the same as django-admin, but is more aware of your project
```

#### python manage.py startapp hello

When we run this command Django creates the skeleton of a web application for us

```
	hello/
	   __init__.py      # This file makes the 'hello' directory into a Python package
	   admin.py         # Administration interface of your app
	   apps.py
	   migrations/      # Django will make files under this based upon our Model
		   __init__.py
	   models.py        # the 'M' in MVC
	   tests.py         # Optional unit tests
	   urls.py          # the 'C' in MVC
	   views.py         # the 'V' in MVC
```


We will want to commit these files to git as well

    $ git add hello
    $ git commit -m "adding Django-generated 'hello' project"


--------------------------------------------------------------------------------
##  2. What's the difference between an *app* and a *project*?


    * Application
        + The code that you actually want to write
        + Fun stuff for your users to enjoy

    * Project
        + Contains applications
        + Framework code which supports your Django apps
        + Applications may be moved between Django projects


--------------------------------------------------------------------------------
##  3. Introduction to Python

Python was created in the mid 1990's, at the height of the Object-Oriented
Programming craze. Its creator, Guido van Rossum is a Monty Python fan; hence
the reference is to the movies, not the snake.

Despite having an inperpreter, Python is a compiled language. However, due to
the inobtrusive nature of the compiler programs written in Python are often
called scripts. Python programs traditionally have a .py extension.

Python has two really great things going for it:

0) The REPL
1) Simple, well-thought out and intuitive syntax

Because of this, it's easy to learn and has become very, very popular.


--------------------------------------------------------------------------------

     _____ _          ___ ___ ___ _    
    |_   _| |_  ___  | _ \ __| _ \ |   
      | | | ' \/ -_) |   / _||  _/ |__ 
      |_| |_||_\___| |_|_\___|_| |____|

The REPL is an interactive environment where you can play with the language
and see for yourself how stuff works.

    R. Read a statement into a string
    E. Evaluate that string as Python code
    P. Print the result
    L. Loop back to the beginning

The idea of the REPL was born in LISP (as so many good ideas were), and all
self-respecting "modern" languages feature this nowadays.

You may enter the REPL simply by running the python executable with no arguments.

You are in the REPL when you see the >>> prompt.

You may also launch the REPL after your script has run by running

    $ python -i scriptname.py


--------------------------------------------------------------------------------

     ___ _            _                   _            
    / __(_)_ __  _ __| |___   ____  _ _ _| |_ __ ___ __
    \__ \ | '  \| '_ \ / -_) (_-< || | ' \  _/ _` \ \ /
    |___/_|_|_|_| .__/_\___| /__/\_, |_||_\__\__,_/_\_\
                |_|              |__/                  

      __   
     /  \  Numbers
    | () | 
     \__/  For the most part, Python isn't too particular about types
           of numbers. All of the familiar arithmetic operations from C++ are
           available: +, -, *, /, %.

               5 + 2
               5 * 2
               5 % 2

           You can explicitly perform integer division with //
           You may perform exponation with **. For example, to raise 2 to the 77th
           power, type

               2 ** 77

     _  
    / |  Strings
    | |  
    |_|  Strings are delimited by double-quotes or single quotes:

         a_string = "this is a\tdouble-quoted string"
         another_string = 'this is a\nsingle-quoted string'
         tough_string = 'she said "my grammer suck\'s"'

         The expected collection of character escape sequences from C are also
         available in Python.

         You may access individual characters in the string with the [] subscript
         operator.

         There is no char data type in Python; what *looks* like a char literal is
         actually a string of length 1.


     ___  
    |_  )  Lists
     / /   
    /___|  Lists are created with square brackets [ ]
           a_list = [1, 2, 3, 4, 5]

           You can access the final element with the offset -1,
           the penultimate element with -2, and so on.

           You can treat lists like a stack data structure by using the .pop() and
           .append() methods:

           a_list.append(6)
           print(a_list.pop())  # prints 6


     ____ 
    |__ /  Loops
     |_ \  
    |___/  for loops iterate over collections. Lists and strings are collection
           types you've seen so far. for loops, and other constructs which
           introduce scope *must* be indented. It is a syntax error to misalign
           your indentation:

           # print each character in tough_string on a line of its own
           for s in tough_string:
    	       print(s)

           # the same, but suppress the newline character that print wants to
           # automagically put at the end of every line
           for s in tough_string:
    	       print(s, end='')


