; newLISP implementation of functional Scheme quicksort
;
; *Note*: letn == let*


; Helper function for partition.
; To create partitions, we need a function which takes as parameters two extra lists:
; the "in" list and the "out" list.
;
; I don't want to force ppl to always have to call partition() with two empty
; lists, so we use this helper function to hide the extra complexity from the
; caller who gets to enjoy a simplified interface.
(define (part-helper pred? lst in out)
  (if (empty? lst)
    (list in out)
    (if (pred? (first lst))
      (part-helper pred? (rest lst) (cons (first lst) in) out)
      (part-helper pred? (rest lst) in (cons (first lst) out)))))

; the user-friendly interface to partition. Simply supply a predicate and a list
(define (part pred? lst)
  (part-helper pred? lst '() '()))


; given a comparator and a list, return a sorted list by way of the quicksort algorithm
; the 2-arity comparator argument is made into a unary predicate and passed to partition
(define (quicksort cmp lst)
  (if (empty? lst)
    '()
    (let ((pivot (first lst))
          (lst (rest lst)))

      (let ((pred? (lambda (n) (cmp n pivot))))

        (let ((parts (part pred? lst)))
          (append (quicksort cmp (first parts)) (list pivot) (quicksort cmp (first (rest parts)))))))))
