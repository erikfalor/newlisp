(define myList '(1 2 3 4 5 6 7 8 9))

(define (rvs+ lst)
  (if (empty? lst)
    lst
    (append (rvs+ (rest lst))
            (list (first lst)))))

(rvs myList)


(define (rvs lst)
    (if (empty? lst)
      lst
      (append (rvs (rest lst)
                   (list (first lst))))))


(rvs myList)
